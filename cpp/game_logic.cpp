#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

Piece::Piece(double piece_length, double piece_radius, double piece_angle, bool piece_switch_on, bool is_bended) : 
length(piece_length), radius(piece_radius), angle(piece_angle), switch_on(piece_switch_on), is_bend(is_bended)
 {}

Bend::Bend(int starting_piece, int ending_piece, int direction, double maximum_speed) : start_piece(starting_piece), end_piece(ending_piece),
bending_direction(direction), max_speed(maximum_speed) {

}

void Map_analyzer::add_piece(double piece_length, double piece_radius, double piece_angle, bool piece_switch_on, bool is_bended) {
  if(is_bended) {
    double kaaren_pituus = circle_length(piece_radius,std::abs(piece_angle));
    std::cout << "kaaren pituus " << kaaren_pituus << std::endl;
    pieces.push_back(Piece(circle_length(piece_radius,std::abs(piece_angle)),piece_radius,piece_angle,piece_switch_on,is_bended));
  } else {
    std::cout << "alkuperäinen pituus " << piece_length << std::endl;
    pieces.push_back(Piece(piece_length,piece_radius,piece_angle,piece_switch_on,is_bended));
  }
}

std::pair<int,int> Map_analyzer::next_turn(const int current_piece) {
  for(auto i = 0; i < bends.size(); i++) {
    int kierros_yli = ((int)((i+1)/bends.size()))*pieces.size();
    if(current_piece <= bends[i].get_end_piece()+kierros_yli) {
      if(current_piece >= bends[i].get_start_piece()+kierros_yli) {
        return std::make_pair(bends[(i+1)%bends.size()].get_bending_direction(),bends[(i+1)%bends.size()].get_start_piece());
      } else {
        return std::make_pair(bends[(i)%bends.size()].get_bending_direction(),bends[(i)%bends.size()].get_start_piece());
      }
    }
  }
  return std::make_pair(0,0);
}

void Map_analyzer::initialize_max_speeds() {
  double next_angle,current_angle = 0;
  int starting_piece = 0, on_bend = 0;
  for (auto i = 1; i < pieces.size(); i++) {
    if(on_bend) {
      next_angle = pieces[i].get_angle();
      if(!pieces[i].get_bending()) {
        bends.push_back(Bend(starting_piece,i-1,(current_angle > 0) - (current_angle < 0),-1));
        on_bend = 0;
      } else if (next_angle*current_angle < 0) { 
        bends.push_back(Bend(starting_piece,i-1,(current_angle > 0) - (current_angle < 0),-1));
        current_angle = next_angle;
        starting_piece = i;
      } else if (i == pieces.size()-1) {
        bends.push_back(Bend(starting_piece,i,(current_angle > 0) - (current_angle < 0),-1));
      }
    } else {
      if(pieces[i].get_bending()) {
        on_bend = 1;
        starting_piece = i;
        current_angle = pieces[i].get_angle();
      }
    }
  }
  std::cout << "Amount of bends " << bends.size() << std::endl;
  for(auto i = 0; i < bends.size(); i++) {
    std::cout << "Bend start " << bends[i].get_start_piece() << " end " << bends[i].get_end_piece() << std::endl;
  }
}

void Map_analyzer::calculate_track_length() {
  track_length = 0;
  for (auto i = 0; i < pieces.size(); i++)
    track_length += pieces[i].get_length();
}

double Map_analyzer::get_location(int current_piece, double current_in_piece_location) {
  double location = current_in_piece_location;
  //std::cout << "Current piece is " << current_piece <<std::endl;
  for(auto i = 0; i < current_piece; i++) {
    location += pieces[i].get_length();
    //std::cout << "Pituus on " << pieces[i].get_length() << std::endl;
  }
  return location;
}

Car::Car() {
  previous_location = -1;
  current_location = 0;
}

/*Car::Car(int starting_lane) : lane(starting_lane) {

}*/

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "gameInit", &game_logic::on_game_init },
      { "yourCar", &game_logic::on_your_car }
    }, switch_message_send(false), next_switch(std::make_pair(0,-1))
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);

  if (msg_type == "carPositions") {
    auto gameTick = msg["gameTick"];
    //std::cout << "Aika on " << gameTick << std::endl;
  }
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  auto amount_of_cars = data.size();
  double current_speed = -1;
  for(auto i = 0; i < amount_of_cars; i++) {
    if(data[i]["id"]["color"] == car.get_color()) {
      car.set_piece(data[i]["piecePosition"]["pieceIndex"].as<int>());
      std::cout << "Current piece is " << car.get_piece() << std::endl;
      if(car.get_previous_location() < 0) {
        car.set_current_location(map.get_location(car.get_piece(),data[i]["piecePosition"]["inPieceDistance"].as<double>()));
        car.set_current_speed(-1);
      } else {
        double cur_loc = map.get_location(car.get_piece(),data[i]["piecePosition"]["inPieceDistance"].as<double>());
        //std::cout << "Current in piece location is " << cur_loc << std::endl;
        car.set_current_location(map.get_location(car.get_piece(),data[i]["piecePosition"]["inPieceDistance"].as<double>()));
        current_speed = car.get_current_location() - car.get_previous_location();
        if(current_speed < 0)
          current_speed += map.get_track_length();
      }
      std::pair<int,int> next_turn = map.next_turn(car.get_piece());
      if (next_turn.first != 0) {
        std::cout << next_turn.first << " " << next_turn.second << std::endl;
        if (next_turn != next_switch) {
          if (next_turn.first < 0 && data[i]["piecePosition"]["lane"]["endLaneIndex"].as<int>() > 0) {
            next_switch = next_turn;
            std::cout << "switch left" << std::endl;
            return { make_switch("Left") };
          } else if (next_turn.first > 0 && data[i]["piecePosition"]["lane"]["endLaneIndex"].as<int>() < map.get_lane_number()) {
            next_switch = next_turn;
            std::cout << "switch right" << std::endl;
            return { make_switch("Right") } ;
          }
        }
      }
      break;
    }
  }
  std::cout << "throttle" << std::endl;
  //auto car = data[0];
  //std::string nimi = car["id"]["name"].as<std::string>();
  //std::cout << "Nimeni oli " << nimi << std::endl;
  //std::cout << "Nopeus on " << current_speed << " locations " << car.get_previous_location() << " " << car.get_current_location() <<  std::endl;
  return { make_throttle(0.6) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
  auto amount_of_pieces = data["race"]["track"]["pieces"].size();
  double length,radius,angle;
  bool switch_on, is_bended;
  for (auto i = 0; i < amount_of_pieces; i++) {
    if(data["race"]["track"]["pieces"][i].get("length") != jsoncons::json::null) {
        length = data["race"]["track"]["pieces"][i]["length"].as<double>();
        //std::cout << "joo " << length << std::endl;
        is_bended = false;
    } else {
        //std::cout << "ei" << std::endl;
        length = 0;
        is_bended = true;
    }
    if(data["race"]["track"]["pieces"][i].get("radius") != jsoncons::json::null) {
        radius = data["race"]["track"]["pieces"][i]["radius"].as<double>();
    } else {
        radius = 0;
    }
    if(data["race"]["track"]["pieces"][i].get("angle") != jsoncons::json::null) {
        angle = data["race"]["track"]["pieces"][i]["angle"].as<double>();
    } else {
        angle = 0;
    }
    if(data["race"]["track"]["pieces"][i].get("switch") != jsoncons::json::null) {
        if (data["race"]["track"]["pieces"][i]["switch"].as<int>() == true) {
          switch_on = true;
          //std::cout << "Löyty switch" << std::endl;
        } else
          switch_on = false;
    } else {
        switch_on = false;
    }
    map.add_piece(length, radius, angle, switch_on, is_bended);
    std::cout << "Pala on " << length << " " << radius << " " << angle << " " << switch_on << std::endl;
  }
  map.calculate_track_length();
  map.initialize_max_speeds();
  map.set_lane_number(data["race"]["track"]["lanes"].size());
  std::cout << "Game has initialized" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
  std::string color = data["color"].as<std::string>();
  car.set_color(color);
  std::cout << "My car color is " << color <<  std::endl;
  return { make_ping() };
}

