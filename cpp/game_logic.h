#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <cmath>
#include <jsoncons/json.hpp>

#define M_PI 3.14159265358979323846

class Piece
{
public:
  Piece(){};
  Piece(double piece_length, double piece_radius, double piece_angle, bool piece_switch_on, bool is_bended);
  double get_angle() { return angle; }
  double get_length() { return length; }
  double get_bending() { return is_bend; }
private:
  double length, radius, angle;
  bool switch_on;
  bool is_bend;
};

class Bend
{
public:
  Bend(int starting_piece, int ending_piece, int bending_direction, double maximum_speed);
  int get_start_piece() { return start_piece; }
  int get_end_piece() { return end_piece; }
  int get_bending_direction() { return bending_direction; }
private:
  int start_piece,end_piece,bending_direction;
  double max_speed;
};

class Map_analyzer
{
public:
  Map_analyzer(){};
  double circle_length(double radius, double angle) { return 2*M_PI*radius*(angle/360.0); }
  void add_piece(double piece_length, double piece_radius, double piece_angle, bool piece_switch_on, bool is_bended);
  std::pair<int,int> next_turn(const int current_piece);
  int set_lane_number(const int number_of_lanes) { amount_of_lanes = number_of_lanes; }
  int get_lane_number() { return amount_of_lanes; }
  void initialize_max_speeds();
  void calculate_track_length();
  double get_location(int current_piece, double current_in_piece_location);
  double get_track_length() { return track_length; }
private:
  std::vector<Piece> pieces;
  std::vector<Bend> bends;
  std::vector<double> max_speed_per_curve;
  int amount_of_lanes;
  double track_length;
};

class Car 
{
public:
  Car();
  //Car(int starting_lane);
  void set_lane(const int current_lane) { lane = current_lane; }
  void set_color(const std::string car_color) { color = car_color; }
  std::string get_color() { return color; }
  void set_piece(const int piece) { current_piece = piece; }
  int get_piece() { return current_piece; }
  void set_current_location(const double loc) { previous_location = current_location; current_location = loc; }
  double get_current_location() { return current_location; }
  double get_previous_location() { return previous_location; }
  void set_current_speed(const double speed) { current_speed = speed; }
  double get_current_speed() { return current_speed; }
private:
  double current_location, previous_location;
  double current_speed, current_mass, current_power;
  int lane, current_piece;
  std::string color;
};

class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  msg_vector react(const jsoncons::json& msg);

private:
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;
  Car car;
  Map_analyzer map;
  int log_level;
  bool switch_message_send;
  std::pair<int,int> next_switch;

  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
  msg_vector on_game_init(const jsoncons::json& data);
  msg_vector on_your_car(const jsoncons::json& data);
};


/*
class car_analyzer
{
public:
private:

  float max_speed;
  float current_speed;

}
*/
#endif
